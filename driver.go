package gocvwinmedia

import (
	"gitee.com/zengyongheng/gocvwinmedia/pkg/driver"
)

// RegisterDriverAdapter allows user space level of driver registration
func RegisterDriverAdapter(a driver.Adapter, info driver.Info) error {
	return driver.GetManager().Register(a, info)
}
